﻿using System;
using System.Collections.Generic;

namespace ZhyveDotnet.AuctionThings.Core.Domain
{
    public class Dialog : BaseEntity
    {
        public virtual User? UserPartner { get; set; }

        public Guid UserPartnerId { get; set; }

        public virtual IList<Message>? Messages { get; set; }

        protected Dialog()
        {
        }
    }
}
