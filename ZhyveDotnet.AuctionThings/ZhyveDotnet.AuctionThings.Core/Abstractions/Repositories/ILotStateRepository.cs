﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories
{
    public interface ILotStateRepository : IRepository<LotState>
    {
        Task<IReadOnlyList<LotState>> GetLotStatesByLotIdAsync(Guid lotId);
    }
}
