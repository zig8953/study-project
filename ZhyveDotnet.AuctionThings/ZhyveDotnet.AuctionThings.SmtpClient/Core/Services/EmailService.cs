﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using ZhyveDotnet.AuctionThings.SmtpClient.Core.Abstractions;

namespace ZhyveDotnet.AuctionThings.SmtpClient.Core.Services
{
    public class EmailService :
        IEmailService
    {
        private MailAddress from;
        private System.Net.Mail.SmtpClient client;

        public EmailService(IConfiguration сonfiguration)
        {
             from = new MailAddress(
               сonfiguration["SmtpSettings:sender"],
               сonfiguration["SmtpSettings:displayName"]);

             client = new System.Net.Mail.SmtpClient(
                сonfiguration["SmtpSettings:address"],
                Convert.ToInt32(сonfiguration["SmtpSettings:port"]));
             client.Credentials = new NetworkCredential(
                сonfiguration["SmtpSettings:sender"],
                сonfiguration["SmtpSettings:password"]);

             client.EnableSsl = true;
        }

        public async Task SendAsync(string recipient, string subject, string notificationMessage)
        {
            MailAddress to = new MailAddress(recipient);
            MailMessage message = new MailMessage(from, to);

            message.Subject = subject;
            message.Body = notificationMessage;

            await client.SendMailAsync(message);
        }
    }
}
