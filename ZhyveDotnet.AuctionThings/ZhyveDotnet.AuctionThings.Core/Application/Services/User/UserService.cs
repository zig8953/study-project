﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services;
using ZhyveDotnet.AuctionThings.Core.Application.Dto.User;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Application.Services
{
    public class UserService
        : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task CreateAsync(UserCreateDto dto)
        {
            User user = new User(dto);
            await _userRepository.CreateAsync(user);
        }

        public async Task<User?> GetUserOrNullAsync(Guid id)
        {
            User? user = await _userRepository.GetByIdAsync(id);
            return user;
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await _userRepository.GetAllAsync();
        }

        public async Task RemoveAsync(Guid id)
        {
            await _userRepository.DeleteAsync(id);
        }

        public async Task UpdateAsync(Guid id, UserUpdateDto dto)
        {
            User? user = await _userRepository.GetByIdAsync(id);

            if (user == null)
            {
                throw new EntityNotFoundException(id);
            }

            user = user.Update(dto);

            await _userRepository.UpdateAsync(user);
        }

        public async Task<User> UpdateRatingAsync(Guid id, UserUpdateRating dto)
        {
            User? user = await _userRepository.GetByIdAsync(id);

            if (user == null)
            {
                throw new EntityNotFoundException(id);
            }

            user = user.UpdateRating(dto);
            await _userRepository.UpdateAsync(user);
            return user;
        }
    }
}
