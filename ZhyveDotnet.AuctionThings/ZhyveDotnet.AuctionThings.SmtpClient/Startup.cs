﻿using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ZhyveDotnet.AuctionThings.SmtpClient.Consumer;
using ZhyveDotnet.AuctionThings.SmtpClient.Core.Abstractions;
using ZhyveDotnet.AuctionThings.SmtpClient.Core.Services;

namespace ZhyveDotnet.AuctionThings.SmtpClient
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddMvcOptions(x =>
                x.SuppressAsyncSuffixInActionNames = false);

            services.AddScoped<IEmailService, EmailService>();

            services.AddMassTransit(config =>
            {
                config.AddConsumer<NotifyWinner>();

                config.AddBus(context => Bus.Factory.CreateUsingRabbitMq(c =>
                {
                    c.UseJsonSerializer();
                    c.Host("localhost", "/", h =>
                    {
                        h.Username("guest");
                        h.Password("guest");
                    });
                    c.ReceiveEndpoint("ZhyveDotnet.AuctionThings.Core.Domain.Contracts:IWinnerNotification", e =>
                    {
                        e.ConfigureConsumer<NotifyWinner>(context);
                    });
                }));
            });

            services.AddMassTransitHostedService();
            services.AddOpenApiDocument(options =>
            {
                options.Title = "SmtpClient API Doc";
                options.Version = "1.0";
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
