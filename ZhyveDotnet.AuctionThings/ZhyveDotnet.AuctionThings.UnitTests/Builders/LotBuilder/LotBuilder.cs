﻿using System;
using System.Collections.Generic;
using System.Text;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.UnitTests.Builders.LotBuilder
{
    public class LotBuilder
    {
        private Lot _lot;

        public LotBuilder()
        {
            _lot = new Lot("Квартира в Минске", 1000, Guid.Parse("d2bea59e-0fce-4e49-a6f2-3dd05e0d042e"), Currency.USD);
        }

        public LotBuilder WithInitialCost(decimal initialCost)
        {
            _lot = new Lot(_lot.Name, initialCost, _lot.UserOwnerId, _lot.Currency);
            return this;
        }

        public LotBuilder ForUser(Guid userId)
        {
            _lot = new Lot(_lot.Name, _lot.InitialCost, userId, _lot.Currency);
            return this;
        }

        public LotBuilder IsDeleted
        {
            get
            {
                _lot.IsDeleted = true;
                return this;
            }
        }

        public Lot Build()
        {
            return _lot;
        }
    }
}
