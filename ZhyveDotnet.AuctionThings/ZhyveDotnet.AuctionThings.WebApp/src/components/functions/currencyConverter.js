﻿export function currencyConverter(n) {
    switch (n) {
        case 1: return 'USD';
        case 2: return 'BYN';
        case 3: return 'RUB';
        case 4: return 'UAH';
        case 5: return 'KZT';
    }
}