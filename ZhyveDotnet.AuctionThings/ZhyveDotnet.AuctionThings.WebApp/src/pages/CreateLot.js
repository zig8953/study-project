﻿import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Input from '../components/Elements/Input';
import Button from '../components/Elements/Button';
import "bootstrap/dist/css/bootstrap.min.css";
import { Form, FormFile } from 'react-bootstrap'

class CreateLot extends React.Component {
    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClearForm = this.handleClearForm.bind(this);
        this.state = {
            LotCreateDto: {
                Name: '',
                InitialCost: '',
                Currency: '1',
                Description: '',
                FileIds: []
            }
        }

        this.handleName = this.handleName.bind(this);
        this.handleInitialCost = this.handleInitialCost.bind(this);
        this.handleCurrency = this.handleCurrency.bind(this);
        this.handleDescription = this.handleDescription.bind(this);
        this.handleChangeFile = this.handleChangeFile.bind(this);
    }

    handleName(e) {
        let value = e.target.value;
        this.setState(prevState => ({
            LotCreateDto: {
                ...prevState.LotCreateDto, Name: value
            }
        }))
    }

    handleInitialCost(e) {
        const re = /[0-9]+[.]?[0-9]*/;
        let value = e.target.value;
        if (e.target.value === '' || re.test(e.target.value)) {
            this.setState(prevState => ({
                LotCreateDto: {
                    ...prevState.LotCreateDto, InitialCost: value
                }
            }))
        }
    }

    handleCurrency(e) {
        let value = e.target.value;
        this.setState(prevState => ({
            LotCreateDto: {
                ...prevState.LotCreateDto, Currency: value
            }
        }))
    }

    handleDescription(e) {
        let value = e.target.value;
        this.setState(prevState => ({
            LotCreateDto: {
                ...prevState.LotCreateDto, Description: value
            }
        }))
    }

    async handleSubmit(event) {
        event.preventDefault();
		const response = await fetch('http://localhost:7000/api/v1/Lot', {
            method: 'POST',
            body: JSON.stringify({
                Name: this.state.LotCreateDto.Name,
                Currency: this.state.LotCreateDto.Currency,
                InitialCost: this.state.LotCreateDto.InitialCost,
                Description: this.state.LotCreateDto.Description,
                FileIds: this.state.LotCreateDto.FileIds
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        });
        await response;
        this.setState({
            LotCreateDto: {
                Name: '',
                InitialCost: '',
                Currency: '1',
                Description: '',
                FileIds: []
            },
        });
    }

    handleClearForm(e) {
        e.preventDefault();
        this.setState({
            LotCreateDto: {
                Name: '',
                InitialCost: '',
                Currency: '1',
                Description: '',
                FileIds: []
            }
        });
    }

    handleChangeFile(e) {
        e.preventDefault();

        if (e.target.files != null && e.target.files.length == 1) {
            var formData = new FormData();

            formData.append("file", e.target.files[0]);

			fetch("http://localhost:7000/api/v1/File",
                {
                    method: 'POST',
                    body: formData
                })
                .then(res => res.json())
                .then(
                    (result) => {
                        console.log(result);

                        var fileIds = this.state.LotCreateDto.FileIds ?? [];
                        fileIds.push(result.id);

                        this.setState(prevState => ({
                            LotCreateDto: {
                                ...prevState.LotCreateDto, FileIds: fileIds
                            }
                        }));
                    },
                    (error) => {
                        console.log(error);
                    }
                )
        };
    }

    render() {
        return (
            <div className="row justify-content-center">
                <form className="container" onSubmit={this.handleSubmit}>
                    <Input inputtype={'text'}
                        name={'Name'}
                        value={this.state.LotCreateDto.Name}
                        placeholder={'Введите название лота'}
                        handleChange={this.handleName}
                    />

                    <Input inputtype={'number'}
                        name={'InitialCost'}
                        value={this.state.LotCreateDto.InitialCost}
                        placeholder={'Начальная стоимость'}
                        handleChange={this.handleInitialCost}
                    />

                    <div className="form-group">
                        <select class="form-control"
                            onChange={this.handleCurrency}
                            name={'Currency'}
                            value={this.state.LotCreateDto.Currency}>
                            <option value="1">USD</option>,
                            <option value="2">EUR</option>,
                            <option value="3">BYN</option>,
                            <option value="4">RUB</option>,
                            <option value="5">UAH</option>,
                            <option value="6">KZT</option>
                        </select>
                    </div>

                    <div className="form-group">
                        <textarea class="form-control"
                            id="exampleFormControlTextarea1"
                            rows="5"
                            placeholder="Описание лота"
                            onChange={this.handleDescription}
                            value={this.state.LotCreateDto.Description}>
                        </textarea>
                    </div>

                    <div className="my-3">
                        <Form.File
                            id="custom-file"
                            label={"Загружено файлов: " + this.state.LotCreateDto.FileIds?.length ?? 0}
                            data-browse="Выбрать файл"
                            onChange={this.handleChangeFile}
                            custom
                        />
                    </div>

                    <Button
                        type={'primary'}
                        title={'Принять'}
                        style={buttonStyle}
                    />

                    <Button
                        action={this.handleClearForm}
                        type={'secondary'}
                        title={'Очистить'}
                        style={buttonStyle}
                    />
                </form>
            </div>
        )
    }
}

const buttonStyle = {
    margin: '10px 10px 10px 10px'
}

export default CreateLot;