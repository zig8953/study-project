﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Application.WinningStrategies
{
    public interface IWinningStrategy
    {
        Bet Determine(IEnumerable<Bet> bets);
    }
}
