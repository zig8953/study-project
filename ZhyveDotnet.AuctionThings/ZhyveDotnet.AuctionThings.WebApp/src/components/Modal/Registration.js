﻿import React from 'react';
import { BaseModal } from './BaseModal';
import { Button, Form } from 'react-bootstrap'

export const Registration = ({ onRequestClose, additionalValue, ...otherProps }) => (
    <BaseModal onRequestClose={onRequestClose} {...otherProps}>
        <h1>Создать учетную запись</h1>
        <br></br>
        <Form.Control type="text" placeholder="Логин" />
        <br></br>
        <Form.Control type="password" placeholder="Пароль" />
        <br></br>
        <Form.Control type="text" placeholder="Телефон" />
        <br></br>

        <Button variant="primary" onClick={onRequestClose}>Создать учетную запись</Button>
  </BaseModal>
);