﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZhyveDotnet.AuctionThings.Core.Application.Dto.User
{
    public class UserUpdateRating
    {
        public byte Rating { get; set; }
    }
}
