﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services;

namespace ZhyveDotnet.AuctionThings.WebApi.Internal
{
    public class CurrentUserContext
        : ICurrentUserContext
    {
        public Guid UserId { get; }

        public string Name { get; }

        public string Email { get; }

        public CurrentUserContext(IHttpContextAccessor httpContextAccessor)
        {
            UserId = default;
            Name = string.Empty;
            Email = string.Empty;

            var claims = httpContextAccessor.HttpContext?.User?.Claims?.ToArray();
            if (claims?.Any() == true)
            {
                if (Guid.TryParse(GetClaimValue(claims, ClaimTypes.NameIdentifier), out var userId))
                {
                    UserId = userId;

                    Name = GetClaimValue(claims, "name");
                    Email = GetClaimValue(claims, "email");
                }
            }
        }

        private string GetClaimValue(IEnumerable<Claim> claims, string type) =>
            claims.FirstOrDefault(x => x.Type == type)?.Value?.ToString() ?? string.Empty;
    }
}