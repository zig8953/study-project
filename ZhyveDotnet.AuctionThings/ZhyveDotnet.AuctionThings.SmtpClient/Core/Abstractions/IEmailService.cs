﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace ZhyveDotnet.AuctionThings.SmtpClient.Core.Abstractions
{
    public interface IEmailService
    {
        public Task SendAsync(string recipient, string subject, string notificationMessage);
    }
}
