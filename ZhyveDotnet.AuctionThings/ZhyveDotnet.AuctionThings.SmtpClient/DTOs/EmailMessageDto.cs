﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZhyveDotnet.AuctionThingsSmtpClient.DTOs
{
    public class EmailMessageDto
    {
        public string Recipient { get; set; }

        public string Body { get; set; }

        public string Subject { get; set; }
    }
}
