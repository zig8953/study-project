﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Services
{
    public interface IBetService
    {
        Task<Bet> CreateBetAsync(Guid userId, Guid lotId, decimal amount);

        Task<Bet?> GetBetOrNullAsync(Guid betId);

        Task DeleteBetAsync(Guid betId);

        Task<IEnumerable<Bet>> GetBetsByLotIdAsync(Guid lotId);

        Task<IEnumerable<Bet>> GetBetsByUserIdAsync(Guid userId);

        Task<Bet> GetBetWinnerForLotOrNullAsync(Guid lotId);

        Task SetBetWinnerForLotAsync(Guid lotId);

        Task<IEnumerable<Bet>> GetBetsByUserIdAndLotIdAsync(Guid lotId, Guid userId);
    }
}
