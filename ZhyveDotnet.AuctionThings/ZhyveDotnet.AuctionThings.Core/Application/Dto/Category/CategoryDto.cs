﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Application.Dto
{
    public class CategoryDto
    {
        public string? Name { get; set; }

        public string? Description { get; set; }

        public bool IsValid()
        {
            if (string.IsNullOrEmpty(Name) && string.IsNullOrEmpty(Description))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
