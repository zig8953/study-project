﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using FluentValidation;
using Moq;
using Xunit;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Application.Dto.User;
using ZhyveDotnet.AuctionThings.Core.Application.Validators;
using ZhyveDotnet.AuctionThings.Core.Domain;
using ZhyveDotnet.AuctionThings.UnitTests.Builders.LotBuilder;
using ZhyveDotnet.AuctionThings.UnitTests.Builders.UserBuilder;

namespace ZhyveDotnet.AuctionThings.UnitTests.Core.Services.BetTests
{
    public class BetValidationTests
    {
        private readonly Mock<IUserRepository> _userRepositoryMock;
        private readonly Mock<ILotRepository> _lotRepositoryMock;
        private readonly BetValidator _betValidator;

        private readonly Guid existingUserId = Guid.Parse("5eb1d37c-3f2f-4748-b0d6-0c4c2fb95b8d");
        private readonly Guid notExistingUserId = Guid.Parse("7219d44d-d8a4-4744-8f1e-fa0bfe91a064");

        private readonly Guid existingLotId = Guid.Parse("28510b82-74e5-4b9d-ac3b-0dca8304fff5");
        private readonly Guid notExistingLotId = Guid.Parse("e034232e-4e2b-4aec-ae05-405c0cec27c5");

        private readonly Lot lot = new LotBuilder().WithInitialCost(3000).Build();

        public BetValidationTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _userRepositoryMock = fixture.Freeze<Mock<IUserRepository>>();
            _lotRepositoryMock = fixture.Freeze<Mock<ILotRepository>>();
            _betValidator = fixture.Build<BetValidator>().Create();
        }

        [Theory]
        [InlineData("00000000-0000-0000-0000-000000000000", "00000000-0000-0000-0000-000000000000", 0)]
        [InlineData("00000000-0000-0000-0000-000000000000", "28510b82-74e5-4b9d-ac3b-0dca8304fff5", 9000)]
        [InlineData("5eb1d37c-3f2f-4748-b0d6-0c4c2fb95b8d", "00000000-0000-0000-0000-000000000000", 9000)]
        [InlineData("5eb1d37c-3f2f-4748-b0d6-0c4c2fb95b8d", "28510b82-74e5-4b9d-ac3b-0dca8304fff5", 0)]
        [InlineData("7219d44d-d8a4-4744-8f1e-fa0bfe91a064", "28510b82-74e5-4b9d-ac3b-0dca8304fff5", 9000)]
        [InlineData("5eb1d37c-3f2f-4748-b0d6-0c4c2fb95b8d", "e034232e-4e2b-4aec-ae05-405c0cec27c5", 9000)]
        [InlineData("5eb1d37c-3f2f-4748-b0d6-0c4c2fb95b8d", "28510b82-74e5-4b9d-ac3b-0dca8304fff5", -999)]
        [InlineData("5eb1d37c-3f2f-4748-b0d6-0c4c2fb95b8d", "28510b82-74e5-4b9d-ac3b-0dca8304fff5", 2000)]
        public void ValidatingBet_BetIsNotValid_ThrowValidationException(Guid userId, Guid lotId, decimal amount)
        {
            Bet bet = new Bet(userId, lotId, amount);
            User user = new UserBuilder().Build();
            _lotRepositoryMock.Setup(l => l.GetByIdAsync(existingLotId)).ReturnsAsync(lot);
            _userRepositoryMock.Setup(u => u.GetByIdAsync(existingUserId)).ReturnsAsync(user);

            Action act = () => _betValidator.ValidateAndThrow(bet);
            act.Should().Throw<ValidationException>();
        }

        [Fact]
        public void ValidatingBet_BetIsValid_NotThrowValidationException()
        {
            Bet bet = new Bet(existingUserId, existingLotId, 6000);
            User user = new UserBuilder().Build();
            _lotRepositoryMock.Setup(l => l.GetByIdAsync(existingLotId)).ReturnsAsync(lot);
            _userRepositoryMock.Setup(u => u.GetByIdAsync(existingUserId)).ReturnsAsync(user);

            Action act = () => _betValidator.ValidateAndThrow(bet);
            act.Should().NotThrow<ValidationException>();
        }

        [Fact]
        public void ValidatingBet_UserDeleted_ThrowValidationException()
        {
            Bet bet = new Bet(existingUserId, existingLotId, 6000);
            User user = new UserBuilder().Build();
            user.IsDeleted = true;
            _lotRepositoryMock.Setup(l => l.GetByIdAsync(existingLotId)).ReturnsAsync(lot);
            _userRepositoryMock.Setup(u => u.GetByIdAsync(existingUserId)).ReturnsAsync(user);

            Action act = () => _betValidator.ValidateAndThrow(bet);
            act.Should().Throw<ValidationException>();
        }

        [Fact]
        public void ValidatingBet_LotDeleted_ThrowValidationException()
        {
            Bet bet = new Bet(existingUserId, existingLotId, 6000);
            User user = new UserBuilder().Build();
            Lot lot = new LotBuilder().WithInitialCost(3000).IsDeleted.Build();
            _lotRepositoryMock.Setup(l => l.GetByIdAsync(existingLotId)).ReturnsAsync(lot);
            _userRepositoryMock.Setup(u => u.GetByIdAsync(existingUserId)).ReturnsAsync(user);

            Action act = () => _betValidator.ValidateAndThrow(bet);
            act.Should().Throw<ValidationException>();
        }
    }
}
