﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Services
{
    public interface ILotStateService
    {
        Task<IEnumerable<LotState>> GetLotStatesByLotIdAsync(Guid lotId);

        Task<LotState?> GetLotStateOrNullAsync(Guid id);

        Task DeleteAsync(Guid id);

        Task<Lot> AddNewStatusInLotAsync(Lot lot, StatusLot statusLot);
    }
}
