﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Application.Providers;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.DataAccess.Repositories
{
    public class LotRepository : ILotRepository
    {
        private readonly DataContext _dataContext;

        public LotRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Lot> CreateAsync(Lot lot)
        {
            await _dataContext.Set<Lot>().AddAsync(lot);
            await _dataContext.SaveChangesAsync();
            return lot;
        }

        public async Task DeleteAsync(Guid id)
        {
            Lot? lot = await _dataContext.Set<Lot>().SingleOrDefaultAsync(x => x.Id == id);
            if (lot == null)
            {
                throw new EntityNotFoundException($"Лот по id {id} не найден.");
            }

            _dataContext.Remove(lot);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<IReadOnlyList<Lot>> GetAllAsync()
        {
            return await _dataContext.Set<Lot>().Where(u => !u.IsDeleted).Include(b => b.Bets).Include(s => s.States).ToListAsync();
        }

        public async Task<Lot?> GetByIdAsync(Guid id)
        {
            return await _dataContext.Set<Lot>()
                .Include(x => x.States)
                .Include(x => x.UserOwner)
                .Include(x => x.Bets)
                .SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task UpdateAsync(Lot lot)
        {
            _dataContext.Set<Lot>().Update(lot);
            await _dataContext.SaveChangesAsync();
        }
    }
}
