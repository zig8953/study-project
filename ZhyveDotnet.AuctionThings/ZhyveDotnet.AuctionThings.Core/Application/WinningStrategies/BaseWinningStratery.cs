﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Application.WinningStrategies
{
    public class BaseWinningStratery : IWinningStrategy
    {
        public Bet Determine(IEnumerable<Bet> bets)
        {
            if (bets == null)
            {
                throw new ArgumentNullException();
            }

            return bets.OrderBy(bet => bet.Amount).LastOrDefault();
        }
    }
}
