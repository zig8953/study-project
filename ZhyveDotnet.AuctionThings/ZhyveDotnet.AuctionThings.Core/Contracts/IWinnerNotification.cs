﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZhyveDotnet.AuctionThings.Core.Domain.Contracts
{
    public interface IWinnerNotification
    {
        public string Email { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
    }
}
