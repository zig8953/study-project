﻿using System;
using System.Collections.Generic;
using ZhyveDotnet.AuctionThings.Core.Application.Dto.User;

namespace ZhyveDotnet.AuctionThings.Core.Domain
{
    public class User
        : BaseEntity
    {
        public string? Login { get; protected set; }

        public string? FirstName { get; protected set; }

        public string? LastName { get; protected set; }

        public DateTime Birthday { get; protected set; }

        public string? Phone { get; protected set; }

        public string? Password { get; protected set; }

        public byte Rating { get; protected set; }

        public virtual Role? Role { get; protected set; }

        public virtual IList<Dialog>? Dialogs { get; protected set; }

        protected User()
        {
            Login = string.Empty;
            Password = string.Empty;
        }

        public User(UserCreateDto dto)
        {
            Id = Guid.NewGuid();
            Login = dto.Login;
            Password = dto.Password;
            Phone = dto.Phone;
        }

        public User Update(UserUpdateDto dto)
        {
            FirstName = dto.FirstName;
            LastName = dto.LastName;
            Birthday = dto.Birthday;
            return this;
        }

        public User UpdateRating(UserUpdateRating dto)
        {
            Rating = dto.Rating;
            return this;
        }
    }
}
