﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ZhyveDotnet.AuctionThings.Core.Domain
{
    public class LotState : BaseEntity
    {
        [Column(TypeName = "varchar(24)")]
        public StatusLot Status { get; protected set; }

        public Guid LotId { get; protected set; }

        public virtual Lot? Lot { get; protected set; }

        protected LotState()
        {
        }

        public LotState(Guid lotId, StatusLot statusLot)
            : base()
        {
            if (lotId == default || statusLot == StatusLot.NotSet)
            {
                throw new ArgumentException();
            }

            Status = statusLot;
            LotId = lotId;
        }
    }
}
