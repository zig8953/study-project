version: '3.4'

networks:
  zhyvedotnet-dev:
    driver: bridge
  elastic:
    driver: bridge

services:
  auction-things-db:
    image: "postgres"
    container_name: 'auction-things-db'
    restart: always
    ports:
      - 5433:5432
    volumes:
      - db_volume:/var/lib/postgresql/data
    environment: 
      POSTGRES_PASSWORD: q2w3e4r5
      POSTGRES_USER: postgres
      POSTGRES_DB: AuctionThingsTestDb
    networks:
      - zhyvedotnet-dev

  zhyvedotnet.auctionthings.webapi:
    image: ${DOCKER_REGISTRY-}zhyvedotnetauctionthingswebapi
    ports:
      - "7000:80"
    build:
      context: ..
      dockerfile: ZhyveDotnet.AuctionThings.WebApi/Dockerfile
    environment:    
      ASPNETCORE_ENVIRONMENT: Development
      ASPNETCORE_URLS: http://+:80
      ConnectionStrings__TestDb: "Host=auction-things-db;Database=AuctionThingsTestDb;Username=postgres;Password=q2w3e4r5;Port=5432"              
    depends_on: 
      - auction-things-db
    networks:
      - zhyvedotnet-dev
      
  zhyvedotnet.rabbitMq:
    image: "rabbitmq:3.8.9-management"
    container_name: 'rabbitMq'
    volumes:
      - 'rabbitmq_data:/data'
    ports:
      - 5672:5672
      - 15672:15672 
      

  zhyvedotnet.auctionthings.smtpclient:
    image: ${DOCKER_REGISTRY-}zhyvedotnetauctionthingssmtpclient
    ports:
      - "8081:80"
    build:
      context: ..
      dockerfile: ZhyveDotnet.AuctionThings.SmtpClient/Dockerfile
    environment:    
      ASPNETCORE_ENVIRONMENT: Development
      ASPNETCORE_URLS: http://+:80
    networks:
      - zhyvedotnet-dev
  
  zhyvedotnet.auctionthings.webapp:
    container_name: webapp
    build:
      context: ../zhyvedotnet.auctionthings.webapp
      dockerfile: Dockerfile
    ports:
      - 3000:80

  zhyvedotnet.auctionthings.apm-server:
    image: docker.elastic.co/apm/apm-server:7.6.2
    depends_on:
      elasticsearch:
        condition: service_healthy
      kibana:
        condition: service_healthy
    cap_add: ["CHOWN", "DAC_OVERRIDE", "SETGID", "SETUID"]
    cap_drop: ["ALL"]
    ports:
      - 8200:8200
    command: >
      apm-server -e
        -E apm-server.rum.enabled=true
        -E setup.kibana.host=kibana:5601
        -E setup.template.settings.index.number_of_replicas=0
        -E apm-server.kibana.enabled=true
        -E apm-server.kibana.host=kibana:5601
        -E output.elasticsearch.hosts=["elasticsearch:9200"]
    healthcheck:
      interval: 10s
      retries: 12
      test: curl --write-out 'HTTP %{http_code}' --fail --silent --output /dev/null http://localhost:8200/

  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:7.6.2
    environment:
      - bootstrap.memory_lock=true
      - cluster.name=docker-cluster
      - cluster.routing.allocation.disk.threshold_enabled=false
      - discovery.type=single-node
      - ES_JAVA_OPTS=-XX:UseAVX=2 -Xms1g -Xmx1g
    ulimits:
      memlock:
        hard: -1
        soft: -1
    volumes:
      - esdata:/usr/share/elasticsearch/data
    ports:
      - 9200:9200
    healthcheck:
      interval: 20s
      retries: 10
      test: curl -s http://localhost:9200/_cluster/health | grep -vq '"status":"red"'

  kibana:
    image: docker.elastic.co/kibana/kibana:7.6.2
    depends_on:
      elasticsearch:
        condition: service_healthy
    environment:
      ELASTICSEARCH_URL: http://elasticsearch:9200
      ELASTICSEARCH_HOSTS: http://elasticsearch:9200
    ports:
      - 5601:5601
    healthcheck:
      interval: 10s
      retries: 20
      test: curl --write-out 'HTTP %{http_code}' --fail --silent --output /dev/null http://localhost:5601/api/status

volumes:
  db_volume:
  rabbitmq_data:
  esdata:
    driver: local
