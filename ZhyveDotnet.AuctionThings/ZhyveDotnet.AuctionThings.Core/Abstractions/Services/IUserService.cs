﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Application.Dto.User;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Services
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetAllAsync();

        Task<User?> GetUserOrNullAsync(Guid id);

        Task CreateAsync(UserCreateDto dto);

        Task UpdateAsync(Guid id, UserUpdateDto dto);

        Task<User> UpdateRatingAsync(Guid id, UserUpdateRating dto);

        Task RemoveAsync(Guid id);
    }
}
