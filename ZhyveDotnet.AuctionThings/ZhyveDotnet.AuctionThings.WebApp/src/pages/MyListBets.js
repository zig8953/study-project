import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from '../components/Elements/Button';
import { Card, CardDeck, CardGroup } from 'react-bootstrap'

class MyListBets extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            bets: []
        };
    }

    componentDidMount() {
		fetch("http://localhost:7000/api/v1/Bet")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        bets: result.map(function (item) {
                            return {
                                id: item.id,
                                name: item.lot.name,
                                isActive: !item.lot.isDeleted && item.lot.winnerBet == null,
                                isWinner: item.isWinner,
                                costInitial: item.lot.initialCost,
                                costBet: item.amount,
                                dateCreated: new Date(Date.parse(item.dateCreated)).toLocaleString(),
                                description: item.lot.shortDescription,
                                mainPhoto: item.lot.mainPhoto != null
                                    ? "/File/Content/" + item.lot.mainPhoto.id + '?privateKey=' + item.lot.mainPhoto.privateKey
                                    : "https://pbs.twimg.com/media/EKI5ZFMU8AAoyWt.jpg"
                            }
                        })
                    });
                },
                (error) => {
                    this.setState({
                        error
                    });
                }
            )
    }

    render() {
        const { error, bets } = this.state;

        if (error) {
            return (
                <div>Ошибка: {error.message}</div>
            )
        } else {
            return (
                <div className="container-fluid">
                    <div className="row justify-content-center">
                        {bets.map(item => (
                            <MyItemBet
                                key={item.id}
                                isActive={item.isActive}
                                isWinner={item.isWinner}
                                header={item.name + ' ' + (item.isWinner ? '👍' : (item.isActive ? '⌛' : '✖️'))}
                                image={item.mainPhoto}
                                title={'От: ' + item.costInitial}
                                subtitle={item.description}
                                text={item.costBet + '!'}
                                footer={item.dateCreated}
                            />
                        ))}
                    </div>
                </div>
            )
        }
    }
}

const MyItemBet = (props) => {
    return (
        <Card border={props.isWinner ? "primary" : (props.isActive ? "info" : "secondary")} className="bg-dark card col-lg-3 m-2 p-2">
            <Card.Header>{props.header}</Card.Header>
            <div
                className="align-self-center p-2"
                style={{ 'min-height': '450px', 'max-height': '450px', 'max-width': '450px' }}
            >
                <Card.Img
                    variant="top"
                    src={props.image}
                    style={!props.isWinner && !props.isActive ? { 'filter': 'grayscale(100%)' } : {}} />
            </div>
            <Card.Body className="p-2">
                <Card.Title className="text-left">{props.title}</Card.Title>
                <Card.Text>{props.text}</Card.Text>
                <Card.Subtitle className="text-right">{props.subtitle}</Card.Subtitle>
            </Card.Body>
            <Card.Footer>
                <div className="align-items-baseline row justify-content-around">
                    <h5 className="text-muted">{props.footer}</h5>
                    <Button
                        type={props.isWinner || props.isActive ? 'primary' : 'secondary'}
                        title={'Открыть'}
                    />
                </div>
            </Card.Footer>
        </Card>
    )
}

export default MyListBets;