﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZhyveDotnet.AuctionThings.Core.Application.Dto.User
{
    public class UserUpdateDto
    {
        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public DateTime Birthday { get; set; }
    }
}
