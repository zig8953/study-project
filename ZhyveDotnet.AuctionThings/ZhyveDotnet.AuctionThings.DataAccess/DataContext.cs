﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ZhyveDotnet.AuctionThings.Core.Application.Providers;
using ZhyveDotnet.AuctionThings.Core.Domain;
using ZhyveDotnet.AuctionThings.Core.Domain.Files;

namespace ZhyveDotnet.AuctionThings.DataAccess
{
    public class DataContext
        : DbContext
    {
        private readonly IDateTimeProvider _dateTimeProvider;

        public DbSet<FileDocument>? FileDocuments { get; set; }

        public DbSet<User>? Users { get; set; }

        public DbSet<Role>? Roles { get; set; }

        public DbSet<Lot>? Lots { get; set; }

        public DbSet<Bet>? Bet { get; set; }

        public DbSet<Category>? Categories { get; set; }

        public DbSet<LotState>? LotStates { get; set; }

        public DbSet<Comment>? Comments { get; set; }

        public DataContext(DbContextOptions<DataContext> options, IDateTimeProvider dateTimeProvider)
            : base(options)
        {
            _dateTimeProvider = dateTimeProvider;
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            OnBeforeSaving();
            return base.SaveChangesAsync(cancellationToken);
        }

        protected void OnBeforeSaving()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    // Write creation date
                    case EntityState.Added when entry.Entity is IDateEntity:
                        entry.Property(nameof(BaseEntity.DateCreated)).CurrentValue = _dateTimeProvider.CurrentDateTime;
                        break;

                    // Soft delete entity
                    case EntityState.Deleted when entry.Entity is IDeletableEntity:
                        entry.State = EntityState.Unchanged;
                        entry.Property(nameof(BaseEntity.DateUpdated)).CurrentValue = _dateTimeProvider.CurrentDateTime;
                        entry.Property(nameof(BaseEntity.IsDeleted)).CurrentValue = true;
                        break;

                    // Write modification date
                    case EntityState.Modified when entry.Entity is IDateEntity:
                        entry.Property(nameof(BaseEntity.DateUpdated)).CurrentValue = _dateTimeProvider.CurrentDateTime;
                        break;
                }
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Lot>().HasMany(x => x.States).WithOne(x => x.Lot).HasForeignKey(x => x.LotId);

            modelBuilder.Entity<Lot>().HasMany(x => x.Bets).WithOne(x => x.Lot).HasForeignKey(x => x.LotId);

            modelBuilder.Entity<Lot>().HasMany(x => x.Photos).WithOne(x => x.Lot).HasForeignKey(x => x.LotId);

            modelBuilder.Entity<Lot>().Property(lot => lot.Currency).HasConversion<string>();
            modelBuilder.Entity<LotState>().Property(state => state.Status).HasConversion<string>();
        }
    }
}