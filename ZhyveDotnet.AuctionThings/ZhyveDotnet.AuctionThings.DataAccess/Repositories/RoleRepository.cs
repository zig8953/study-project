﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Application.Providers;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.DataAccess.Repositories
{
    public class RoleRepository
        : IRoleRepository
    {
        private readonly DataContext _dataContext;
        private readonly IConfiguration _configuration;

        public RoleRepository(DataContext dataContext, IDateTimeProvider dateTimeProvider, IConfiguration configuration)
        {
            _dataContext = dataContext;
            _configuration = configuration;
        }

        public async Task<Role> CreateAsync(Role role)
        {
            await _dataContext.Set<Role>().AddAsync(role);
            await _dataContext.SaveChangesAsync();
            return role;
        }

        public async Task DeleteAsync(Guid id)
        {
            Role? role = await _dataContext.Set<Role>().SingleOrDefaultAsync(x => x.Id.Equals(id));
            if (role == null)
            {
                throw new EntityNotFoundException($"Роль по id {id} не найден.");
            }

            _dataContext.Remove(role);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<IReadOnlyList<Role>> GetAllAsync()
        {
            return await _dataContext.Set<Role>().ToListAsync();
        }

        public async Task<Role?> GetByIdAsync(Guid id)
        {
            return await _dataContext.Set<Role>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task UpdateAsync(Role role)
        {
            _dataContext.Set<Role>().Update(role);
            await _dataContext.SaveChangesAsync();
        }
    }
}
