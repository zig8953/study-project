﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories.Files;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services.Files;
using ZhyveDotnet.AuctionThings.Core.Application.Dto;
using ZhyveDotnet.AuctionThings.Core.Contracts;
using ZhyveDotnet.AuctionThings.Core.Domain;
using ZhyveDotnet.AuctionThings.Core.Domain.Contracts;
using ZhyveDotnet.AuctionThings.Core.Domain.Files;

namespace ZhyveDotnet.AuctionThings.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class LotController : Controller
    {
        private readonly ICurrentUserContext _currentUserContext;
        private readonly ILotService _lotService;
        private readonly IBetService _betService;
        private readonly IFileDocumentService _fileDocumentService;
        private readonly IUserService _usersService;
        private readonly IRequestClient<IWinnerNotification> _winnerNotificationClient;

        public LotController(
            ICurrentUserContext currentUserContext,
            ILotService lotService,
            IBetService betService,
            IFileDocumentService fileDocumentService,
            IUserService users,
            IRequestClient<IWinnerNotification> notificationClient)
        {
            _currentUserContext = currentUserContext;
            _lotService = lotService;
            _betService = betService;
            _fileDocumentService = fileDocumentService;
            _usersService = users;
            _winnerNotificationClient = notificationClient;
        }

        /// <summary>
        /// Создание нового лота.
        /// </summary>
        /// <param name="lotCreateDto">Данные нового лота.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateLotAsync([FromBody] LotCreateDto lotCreateDto)
        {
            lotCreateDto.UserOwnerId = _currentUserContext.UserId;

            if (lotCreateDto.FileIds?.Any() == true)
            {
                var files = await _fileDocumentService
                    .GetFileCollectionAsync(new FileDocumentFilter
                    {
                        Ids = lotCreateDto.FileIds
                    })
                    .ConfigureAwait(false);

                lotCreateDto.Photos = files is FileDocument[] filesArr ? filesArr : files.ToArray();
            }

            var id = await _lotService.CreateAsync(lotCreateDto);
            return Ok(id);
        }

        /// <summary>
        /// Получить все лоты.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpGet]
        public async Task<IEnumerable<Lot>> GetLotsAsync()
        {
            return await _lotService.GetAllAsync();
        }

        /// <summary>
        /// Получить лот или пустое значение.
        /// </summary>
        /// <param name="id">Id лота.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<Lot?>> GetLotAsync(Guid id)
        {
            var lot = await _lotService.GetLotOrNullAsync(id);
            if (lot == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(lot);
            }
        }

        /// <summary>
        /// Удалить лот.
        /// </summary>
        /// <param name="id">Id лота.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> RemoveLotAsync(Guid id)
        {
            await _lotService.DeleteAsync(id);
            return Ok();
        }

        /// <summary>
        /// Обновить информацию о лоте.
        /// </summary>
        /// <param name="id">Id обновляемого лота.</param>
        /// <param name="lotUpdateDto">Данные обновленного лота.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpPut]
        public async Task<ActionResult> EditLotAsync(Guid id, LotUpdateDto lotUpdateDto)
        {
            await _lotService.UpdateAsync(id, lotUpdateDto);
            return Ok();
        }

        /// <summary>
        /// Закончить аукцион и определить победителя.
        /// </summary>
        /// <param name="closeLotId">Id закрываемого аукциона.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpPut("{closeLotId:guid}")]
        public async Task<ActionResult> CloseAuctionAsync(Guid closeLotId)
        {
            if (closeLotId == default)
            {
                return BadRequest("Отстутствует id лота.");
            }

            await _betService.SetBetWinnerForLotAsync(closeLotId);

            var winnerBet = await _betService.GetBetWinnerForLotOrNullAsync(closeLotId);
            var userInfo = await _usersService.GetUserOrNullAsync(winnerBet.UserBidderId);
            if (userInfo == null || userInfo?.Login == null)
            {
                return BadRequest("Победитель не найден");
            }

            var notificationMessage =
                new WinnerNotificationMessage(userInfo.Login, "Ваша ставка выиграла", closeLotId.ToString());
            await _winnerNotificationClient.GetResponse<EmptyResponse>(notificationMessage);

            return Ok();
        }
    }
}