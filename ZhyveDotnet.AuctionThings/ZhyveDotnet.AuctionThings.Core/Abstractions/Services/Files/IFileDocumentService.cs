﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories.Files;
using ZhyveDotnet.AuctionThings.Core.Domain.Files;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Services.Files
{
    public interface IFileDocumentService
    {
        Task<FileDocument?> CreateAsync(string fileName, Stream stream, Guid userId);

        Task<FileDocument?> GetFileAsync(Guid id);

        Task<IReadOnlyList<FileDocument>> GetFileCollectionAsync(FileDocumentFilter? filter);

        Task<Stream?> GetFileContentAsync(FileDocument file, string privateKey);

        Task UpdateAsync(Guid id, string fileName, Stream stream);
    }
}