﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZhyveDotnet.AuctionThings.DataAccess.Migrations
{
    public partial class FileUserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lots_Bet_CurrentBetId",
                table: "Lots");

            migrationBuilder.DropForeignKey(
                name: "FK_Lots_LotStates_CurrentStateId",
                table: "Lots");

            migrationBuilder.DropForeignKey(
                name: "FK_Lots_FileDocuments_MainPhotoId",
                table: "Lots");

            migrationBuilder.DropIndex(
                name: "IX_Lots_CurrentBetId",
                table: "Lots");

            migrationBuilder.DropIndex(
                name: "IX_Lots_CurrentStateId",
                table: "Lots");

            migrationBuilder.DropIndex(
                name: "IX_Lots_MainPhotoId",
                table: "Lots");

            migrationBuilder.DropColumn(
                name: "CurrentBetId",
                table: "Lots");

            migrationBuilder.DropColumn(
                name: "CurrentStateId",
                table: "Lots");

            migrationBuilder.DropColumn(
                name: "MainPhotoId",
                table: "Lots");

            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "FileDocuments",
                nullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "ParentId",
                table: "Categories",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.CreateIndex(
                name: "IX_FileDocuments_UserId",
                table: "FileDocuments",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_FileDocuments_Users_UserId",
                table: "FileDocuments",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FileDocuments_Users_UserId",
                table: "FileDocuments");

            migrationBuilder.DropIndex(
                name: "IX_FileDocuments_UserId",
                table: "FileDocuments");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "FileDocuments");

            migrationBuilder.AddColumn<Guid>(
                name: "CurrentBetId",
                table: "Lots",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CurrentStateId",
                table: "Lots",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "MainPhotoId",
                table: "Lots",
                type: "uuid",
                nullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "ParentId",
                table: "Categories",
                type: "uuid",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Lots_CurrentBetId",
                table: "Lots",
                column: "CurrentBetId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Lots_CurrentStateId",
                table: "Lots",
                column: "CurrentStateId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Lots_MainPhotoId",
                table: "Lots",
                column: "MainPhotoId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Lots_Bet_CurrentBetId",
                table: "Lots",
                column: "CurrentBetId",
                principalTable: "Bet",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Lots_LotStates_CurrentStateId",
                table: "Lots",
                column: "CurrentStateId",
                principalTable: "LotStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Lots_FileDocuments_MainPhotoId",
                table: "Lots",
                column: "MainPhotoId",
                principalTable: "FileDocuments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
