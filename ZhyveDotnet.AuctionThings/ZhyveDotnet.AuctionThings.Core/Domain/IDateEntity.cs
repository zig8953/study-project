﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZhyveDotnet.AuctionThings.Core.Domain
{
    public interface IDateEntity
    {
        public DateTime DateUpdated { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
