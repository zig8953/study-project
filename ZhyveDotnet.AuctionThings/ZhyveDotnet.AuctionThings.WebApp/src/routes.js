import { MainLayout } from './layouts/main';

import { Home } from './pages/Home';
import LotCardsView from './pages/LotCardsView';
import MyListBets from './pages/MyListBets';
import LotDetails from './pages/LotDetails';

export const routes = [
    {
        component: MainLayout,
        routes: [
            {
                path: "/",
                exact: true,
                component: LotCardsView
            },
            {
                path: "/LotCardsView",
                component: LotCardsView
            },
            {
                path: "/MyListBets",
                component: MyListBets
            },
            {
                path: "/LotDetails",
                component: LotDetails
            }
        ]
    }
];