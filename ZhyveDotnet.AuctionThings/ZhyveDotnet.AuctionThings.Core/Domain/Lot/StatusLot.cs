﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ZhyveDotnet.AuctionThings.Core.Domain
{
    public enum StatusLot
    {
        /// <summary>
        /// Не заданно
        /// </summary>
        [Display(Name = "Не заданно")]
        NotSet,

        /// <summary>
        /// Черновик
        /// </summary>
        [Display(Name = "Черновик")]
        Draft,

        /// <summary>
        /// Торги
        /// </summary>
        [Display(Name = "Торги")]
        Bidding,

        /// <summary>
        /// Завершение торгов
        /// </summary>
        [Display(Name = "Завершение торгов")]
        Completed,

        /// <summary>
        /// Подтверждение оплаты
        /// </summary>
        [Display(Name = "Подтверждение оплаты")]
        PaymentConfirmation,

        /// <summary>
        /// Подтверждение получения товара
        /// </summary>
        [Display(Name = "Подтверждение получения товара")]
        ConfirmationReceiptProduct,

        /// <summary>
        /// Архив
        /// </summary>
        [Display(Name = "Архив")]
        Archive
    }
}
