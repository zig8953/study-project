﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Application.Providers;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.DataAccess.Repositories
{
    public class CommentRepository : ICommentRepository
    {
        private readonly DataContext _dataContext;

        public CommentRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Comment> CreateAsync(Comment entity)
        {
            await _dataContext.Set<Comment>().AddAsync(entity);
            await _dataContext.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(Guid id)
        {
            Comment? сomment = await _dataContext.Set<Comment>().SingleOrDefaultAsync(x => x.Id == id);
            if (сomment == null)
            {
                throw new EntityNotFoundException($"Комментарий по id {id} не найден.");
            }

            _dataContext.Remove(сomment);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<IReadOnlyList<Comment>> GetAllAsync()
        {
            return await _dataContext.Set<Comment>().ToListAsync();
        }

        public async Task<IReadOnlyList<Comment>> GetAllByLotIdAsync(Guid lotId)
        {
            return await _dataContext.Set<Comment>().Where(x => x.LotId == lotId).ToListAsync();
        }

        public async Task<Comment?> GetByIdAsync(Guid id)
        {
            return await _dataContext.Set<Comment>().SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task UpdateAsync(Comment value)
        {
            _dataContext.Set<Comment>().Update(value);
            await _dataContext.SaveChangesAsync();
        }
    }
}