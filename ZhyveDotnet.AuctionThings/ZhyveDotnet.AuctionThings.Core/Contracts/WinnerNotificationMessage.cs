﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZhyveDotnet.AuctionThings.Core.Domain.Contracts
{
    public class WinnerNotificationMessage : IWinnerNotification
    {
        public WinnerNotificationMessage(string email, string subject, string body)
        {
            Email = email;
            Subject = subject;
            Body = body;
        }

        public string Email { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
    }
}
