﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZhyveDotnet.AuctionThings.Core.Domain
{
    public class Role : BaseEntity
    {
        public string? Name { get; set; }

        public string? Description { get; set; }

        protected Role()
        {
        }
    }
}
