﻿namespace ZhyveDotnet.AuctionThings.DataAccess
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}