﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;

namespace ZhyveDotnet.AuctionThings.WebApi.Middlewares
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        private Dictionary<Type, int> exceptionTypeStatusCodePairs = new Dictionary<Type, int>()
        {
            { typeof(EntityNotFoundException), StatusCodes.Status400BadRequest },
            { typeof(WrongBetException), StatusCodes.Status400BadRequest },
            { typeof(InvalidOperationException), StatusCodes.Status400BadRequest },
            { typeof(ArgumentException), StatusCodes.Status422UnprocessableEntity },
            { typeof(ArgumentNullException), StatusCodes.Status400BadRequest },
            { typeof(ValidationException), StatusCodes.Status422UnprocessableEntity }
        };

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, ILogger<ExceptionHandlingMiddleware> logger, IWebHostEnvironment hostingEnvironment)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                if (context.Response.HasStarted)
                {
                    logger.LogWarning(
                        ex,
                        "The response has already started, the http status code middleware will not be executed.");

                    throw;
                }

                context.Response.Clear();
                context.Response.ContentType = "application/json";

                object error = new
                {
                    error = ex.Message
                };
                int statusCode;

                if (exceptionTypeStatusCodePairs.ContainsKey(ex.GetType()))
                {
                    statusCode = exceptionTypeStatusCodePairs[ex.GetType()];
                }
                else
                {
                    statusCode = StatusCodes.Status500InternalServerError;
                    var isDevelopment = hostingEnvironment.IsDevelopment();

                    error = new
                    {
                        error = isDevelopment ? ex.Message : "An error occurred during processing your request",
                        stackTrace = isDevelopment ? ex.StackTrace : null
                    };
                }

                context.Response.StatusCode = statusCode;
                var text = JsonSerializer.Serialize(error);

                await context.Response.WriteAsync(text);
            }
        }
    }
}
