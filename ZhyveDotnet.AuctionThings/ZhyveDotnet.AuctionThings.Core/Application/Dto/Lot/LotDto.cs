﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZhyveDotnet.AuctionThings.Core.Application.Dto.Lot
{
    public class LotDto
    {
        public string? Name { get; set; }

        public string? Description { get; set; }

        public string? ShortDescription { get; set; }
    }
}
