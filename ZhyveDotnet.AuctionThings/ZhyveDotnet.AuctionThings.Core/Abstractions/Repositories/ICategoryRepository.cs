﻿using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}
