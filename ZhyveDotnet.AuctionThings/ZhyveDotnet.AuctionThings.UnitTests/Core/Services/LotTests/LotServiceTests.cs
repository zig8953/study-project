﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using Xunit;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services;
using ZhyveDotnet.AuctionThings.Core.Application.Dto;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Application.Services;
using ZhyveDotnet.AuctionThings.Core.Domain;
using ZhyveDotnet.AuctionThings.UnitTests.Builders.LotBuilder;

namespace ZhyveDotnet.AuctionThings.UnitTests.Core.Services.LotTests
{
    public class LotServiceTests
    {
        private readonly Mock<ILotRepository> _lotRepositoryMock;
        private readonly ILotService _lotService;

        public LotServiceTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _lotRepositoryMock = fixture.Freeze<Mock<ILotRepository>>();
            _lotService = fixture.Build<LotService>().Create();
        }

        [Fact]
        public async Task GetAllAsync_GetAll_AllLotsIsReceivedAsync()
        {
            var lots = new List<Lot>() { new LotBuilder().Build(), new LotBuilder().Build() };
            _lotRepositoryMock.Setup(repo => repo.GetAllAsync()).ReturnsAsync(lots);
            var result = await _lotService.GetAllAsync();

            result.Should().BeEquivalentTo(lots);
        }

        [Fact]
        public async Task GetLotOrNullAsync_LotIsExist_LotReceivedAsync()
        {
            var expectedLot = new LotBuilder().Build();
            _lotRepositoryMock.Setup(repo => repo.GetByIdAsync(expectedLot.Id)).ReturnsAsync(expectedLot);
            var lot = await _lotService.GetLotOrNullAsync(expectedLot.Id);

            lot.Should().BeEquivalentTo(expectedLot);
        }

        [Fact]
        public async Task GetLotOrNullAsync_LotIsNotExist_LotNotReceivedAsync()
        {
            var lotId = Guid.NewGuid();
            var lot = await _lotService.GetLotOrNullAsync(lotId);

            lot.Should().BeNull();
        }

        [Fact]
        public async Task UpdateAsync_LotNotFound_EntityNotFoundExceptionAsync()
        {
            Guid lotId = Guid.NewGuid();
            Func<Task> act = async () => await _lotService.UpdateAsync(lotId, new LotUpdateDto());
            await act.Should().ThrowAsync<EntityNotFoundException>($"Сущность с id {lotId} не найдена");
        }

        [Fact]
        public async Task UpdateAsync_LotUpdate_LotNameUpdatedAsync()
        {
            var lot = new LotBuilder().Build();
            _lotRepositoryMock.Setup(r => r.GetByIdAsync(lot.Id)).ReturnsAsync(lot);
            string newName = "NewName";
            LotUpdateDto dto = new LotUpdateDto() { Name = newName };

            await _lotService.UpdateAsync(lot.Id, dto);
            lot.Name.Should().BeEquivalentTo(newName);
        }
    }
}
