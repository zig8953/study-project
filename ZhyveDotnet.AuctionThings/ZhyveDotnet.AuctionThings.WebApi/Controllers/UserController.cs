﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services;
using ZhyveDotnet.AuctionThings.Core.Application.Dto.User;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Получить данные всех пользователях.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpGet]
        public async Task<IEnumerable<User>> GetUsersAsync()
        {
            return await _userService.GetAllAsync();
        }

        /// <summary>
        /// Получить пользователя.
        /// </summary>
        /// <param name="id">Пользователя.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<User>> GetUserAsync(Guid id)
        {
            if (id == default(Guid))
            {
                return BadRequest("Отсутствуют id пользователя.");
            }

            var user = await _userService.GetUserOrNullAsync(id);

            if (user == null)
            {
                return NotFound("Пользователь с таким id не найден");
            }

            return Ok(user);
        }

        /// <summary>
        /// Добавление нового пользователя.
        /// </summary>
        /// <param name="dto">модель.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpPost("Create")]
        public async Task<ActionResult<IEnumerable<User>>> CreateAsync(UserCreateDto dto)
        {
            await _userService.CreateAsync(dto);
            return Ok();
        }

        /// <summary>
        /// Обновить пользователя.
        /// </summary>
        /// <param name="id">Id обновляемого пользователя.</param>
        /// <param name="dto">Данные обновленного пользователя.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpPut("Update")]
        public async Task<ActionResult> UpdateAsync(Guid id, UserUpdateDto dto)
        {
            if (id == default(Guid))
            {
                return BadRequest("Отсутствуют id пользователя.");
            }

            await _userService.UpdateAsync(id, dto);
            return Ok();
        }

        /// <summary>
        /// Обновить стус.
        /// </summary>
        /// <param name="id">Id обновляемого пользователя.</param>
        /// <param name="dto">Данные рейтинга обновленного пользователя.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpPut("UpdateRating")]
        public async Task<ActionResult<IEnumerable<User>>> UpdateRatingAsync(Guid id, UserUpdateRating dto)
        {
            if (id == default(Guid))
            {
                return BadRequest("Отсутствуют id пользователя.");
            }

            await _userService.UpdateRatingAsync(id, dto);
            return Ok();
        }

        /// <summary>
        /// Удалить пользователя.
        /// </summary>
        /// <param name="id">модель.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> RemoveAsync(Guid id)
        {
            if (id == default(Guid))
            {
                return BadRequest("Отсутствуют id пользователя.");
            }

            await _userService.RemoveAsync(id);
            return Ok();
        }
    }
}
