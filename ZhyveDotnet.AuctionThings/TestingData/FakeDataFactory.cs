﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZhyveDotnet.AuctionThings.Core.Application.Dto;
using ZhyveDotnet.AuctionThings.Core.Application.Dto.User;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.DataAccess
{
    public static class FakeDataFactory
    {
        private static readonly Lot Lot1;
        private static readonly Lot Lot2;

        private static readonly User User1;
        private static readonly User User2;

        private static readonly Bet Bet1;
        private static readonly Bet Bet2;

        static FakeDataFactory()
        {
            User1 = new User(new UserCreateDto() { Login = "testzhyvedontnet@mail.kz", Password = "12qwasZX", Phone = "+375447788778" });
            User1.Id = new Guid("171129af-a968-4cbf-9acc-edaad8a740fc");

            User2 = new User(new UserCreateDto() { Login = "anotheruser@mail.by", Password = "qwerty12", Phone = "+375291112233" });
            User2.Id = new Guid("0ed2fc6b-817c-44a8-8ee9-425fb199e454");

            Lot1 = new Lot(new LotCreateDto()
            {
                Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                Name = "Квартира в Минске2",
                InitialCost = 1000,
                UserOwnerId = User1.Id,
                Currency = Currency.EUR,
                ShortDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
            });

            Lot2 = new Lot(new LotCreateDto()
            {
                Description = "Воспитанные люди должны удовлетворять следующим условиям: " +
                    "… Они уважают человеческую личность, всегда снисходительны, мягкие, вежливые, уступчивые… " +
                    "… Они уважают чужую собственность, а потому платят долги. " +
                    "… Не лгут даже в пустяках… Они не лезут с откровенностями, когда их не спрашивают… " +
                    "… Они не унижают себя с тою целью, чтобы вызвать в другом сочувствие…" +
                    "… Они не суетны… " +
                    "… Если имеют в себе талант, то уважают его… Они жертвуют для него всем…" +
                    "… Они воспитывают в себе эстетику… " +
                    "… Тут нужны беспрерывные дневной и ночной труд, вечное чтение, воля… Тут дорог каждый час.",
                Name = "Мертвые души. 2 Том",
                InitialCost = 3000,
                UserOwnerId = User2.Id,
                Currency = Currency.KZT,
                ShortDescription = "Воспитанные люди должны удовлетворять следующим условиям:"
            });

            Bet1 = new Bet(User1.Id, Lot2.Id, 4000);

            Bet2 = new Bet(User2.Id, Lot1.Id, 2000);
        }

        public static List<Lot> Lots => new List<Lot>() { Lot1, Lot2 };

        public static List<LotState> LotStates => new List<LotState>()
        {
            new LotState(Lot1.Id, StatusLot.Bidding),
            new LotState(Lot2.Id, StatusLot.Bidding)
        };

        public static List<User> Users => new List<User>() { User1, User2 };

        public static List<Bet> Bets => new List<Bet>() { Bet1, Bet2 };
    }
}