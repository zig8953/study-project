﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RoleController : Controller
    {
        private readonly IRoleService _roleService;

        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        /// <summary>
        /// Получить все роли.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpGet]
        public async Task<IEnumerable<Role>> GetRolesAsync()
        {
            return await _roleService.GetAllAsync();
        }

        /// <summary>
        /// Получить роль.
        /// </summary>
        /// <param name="id">Роли.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<Role>> GetRoleAsync(Guid id)
        {
            if (id == default(Guid))
            {
                return BadRequest("Отсутствуют id роли.");
            }

            var role = await _roleService.GetLotOrNullAsync(id);

            if (role == null)
            {
                return NotFound("роль с таким id не найден");
            }

            return Ok(role);
        }

        /// <summary>
        /// Добавление новой роли.
        /// </summary>
        /// <param name="role"> Роль.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpPost("Create")]
        public async Task<ActionResult<IEnumerable<User>>> CreateAsync(Role role)
        {
            await _roleService.CreateAsync(role);
            return Ok();
        }

        /// <summary>
        /// Обновление роли.
        /// </summary>
        /// <param name="id">Id обновляемой роли.</param>
        /// <param name="role">Данные для обновление.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpPut("Update")]
        public async Task<ActionResult> UpdateAsync(Guid id, Role role)
        {
            if (id == default(Guid))
            {
                return BadRequest("Отсутствуют id роли.");
            }

            await _roleService.UpdateAsync(id, role);
            return Ok();
        }

        /// <summary>
        /// Удаление роли.
        /// </summary>
        /// <param name="id">Роли.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> RemoveAsync(Guid id)
        {
            if (id == default(Guid))
            {
                return BadRequest("Отсутствуют id роли.");
            }

            await _roleService.DeleteAsync(id);
            return Ok();
        }
    }
}
