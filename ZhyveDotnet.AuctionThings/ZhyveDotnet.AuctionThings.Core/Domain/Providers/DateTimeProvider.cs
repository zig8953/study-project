﻿using System;
using System.Collections.Generic;
using System.Text;
using ZhyveDotnet.AuctionThings.Core.Application.Providers;

namespace ZhyveDotnet.AuctionThings.Core.Domain.Providers
{
    public class DateTimeProvider
        : IDateTimeProvider
    {
        public DateTime CurrentDateTime => DateTime.Now;
    }
}
