﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZhyveDotnet.AuctionThings.DataAccess.Migrations
{
    public partial class Lot_Update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dialog_Users_UserId",
                table: "Dialog");

            migrationBuilder.DropIndex(
                name: "IX_Dialog_UserId",
                table: "Dialog");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Dialog");

            migrationBuilder.AddColumn<string>(
                name: "Content",
                table: "Message",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Status",
                table: "LotStates",
                type: "varchar(24)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<string>(
                name: "Currency",
                table: "Lots",
                type: "varchar(24)",
                nullable: false,
                defaultValue: " ");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Lots",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UserPartnerId",
                table: "Dialog",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AlterColumn<Guid>(
                name: "ParentId",
                table: "Categories",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Dialog_UserPartnerId",
                table: "Dialog",
                column: "UserPartnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dialog_Users_UserPartnerId",
                table: "Dialog",
                column: "UserPartnerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dialog_Users_UserPartnerId",
                table: "Dialog");

            migrationBuilder.DropIndex(
                name: "IX_Dialog_UserPartnerId",
                table: "Dialog");

            migrationBuilder.DropColumn(
                name: "Content",
                table: "Message");

            migrationBuilder.DropColumn(
                name: "Currency",
                table: "Lots");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Lots");

            migrationBuilder.DropColumn(
                name: "UserPartnerId",
                table: "Dialog");

            migrationBuilder.AlterColumn<int>(
                name: "Status",
                table: "LotStates",
                type: "integer",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(24)");

            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "Dialog",
                type: "uuid",
                nullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "ParentId",
                table: "Categories",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.CreateIndex(
                name: "IX_Dialog_UserId",
                table: "Dialog",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dialog_Users_UserId",
                table: "Dialog",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
