﻿using System;

namespace ZhyveDotnet.AuctionThings.Core.Domain
{
    public class Comment : BaseEntity
    {
        public Guid UserAuthorId { get; set; }

        public virtual User? UserAuthor { get; set; }

        public string? Content { get; set; }

        public Guid LotId { get; set; }

        public virtual Lot? Lot { get; set; }

        protected Comment()
        {
        }

        public Comment(Guid userAuthorId, Guid lotId, string? content)
        {
            UserAuthorId = userAuthorId;
            LotId = lotId;
            Content = content;
        }

        public Comment Update(string? content)
        {
            this.Content = content;
            return this;
        }
    }
}