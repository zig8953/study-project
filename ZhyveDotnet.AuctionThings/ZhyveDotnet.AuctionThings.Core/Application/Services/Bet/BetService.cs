﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Application.WinningStrategies;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Application.Services
{
    public class BetService : IBetService
    {
        private readonly IBetRepository _betRepository;
        private readonly IWinningStrategy _winningStrategy;
        private readonly ILotRepository _lotRepository;
        private readonly ILotStateService _lotStateService;
        private readonly IValidator<Bet> _validator;

        public BetService(IBetRepository betRepository, IWinningStrategy winnerStrategy, ILotRepository lotRepository, ILotStateService lotStateService, IValidator<Bet> validator)
        {
            _betRepository = betRepository;
            _winningStrategy = winnerStrategy;
            _lotRepository = lotRepository;
            _lotStateService = lotStateService;
            _validator = validator;
        }

        public async Task<Bet> CreateBetAsync(Guid userId, Guid lotId, decimal amount)
        {
            Bet bet = new Bet(userId, lotId, amount);
            _validator.ValidateAndThrow(bet);

            var placedBets = await _betRepository.GetBetsByLotIdAsync(lotId);
            if (placedBets != null && placedBets.Count > 0)
            {
                if (placedBets.Any(b => b.IsWinner))
                {
                    throw new WrongBetException("Ошибка операции - невозможно совершить ставку, так как торги уже были закрыты.");
                }

                var leadingBet = _winningStrategy.Determine(placedBets);
                if (leadingBet != null && leadingBet.Amount >= amount)
                {
                    throw new WrongBetException($"Ошибка операции - невозможно совершить ставку, " +
                    $"так как новая ставка не превышает размер текущей.");
                }
            }

            await _betRepository.CreateAsync(bet);
            return bet;
        }

        public async Task DeleteBetAsync(Guid betId)
        {
            await _betRepository.DeleteAsync(betId);
        }

        public async Task<Bet?> GetBetOrNullAsync(Guid betId)
        {
            return await _betRepository.GetByIdAsync(betId);
        }

        public async Task<IEnumerable<Bet>> GetBetsByLotIdAsync(Guid lotId)
        {
            return await _betRepository.GetBetsByLotIdAsync(lotId);
        }

        public async Task<IEnumerable<Bet>> GetBetsByUserIdAndLotIdAsync(Guid lotId, Guid userId)
        {
            return await _betRepository.GetBetsByUserIdAndLotIdAsync(lotId, userId);
        }

        public async Task<IEnumerable<Bet>> GetBetsByUserIdAsync(Guid userId)
        {
            return await _betRepository.GetBetsByUserIdAsync(userId);
        }

        public async Task<Bet> GetBetWinnerForLotOrNullAsync(Guid lotId)
        {
            var bets = await _betRepository.GetBetsByLotIdAsync(lotId);
            return bets?.SingleOrDefault(bet => bet.IsWinner);
        }

        public async Task SetBetWinnerForLotAsync(Guid lotId)
        {
            var lot = await _lotRepository.GetByIdAsync(lotId);
            if (lot == null)
            {
                throw new EntityNotFoundException(lotId);
            }

            var bets = await _betRepository.GetBetsByLotIdAsync(lotId);

            if (bets != null && bets.Count > 0)
            {
                if (bets.Any(b => b.IsWinner))
                {
                    throw new InvalidOperationException("Ошибка операции - торги уже были закрыты.");
                }

                var winnerBet = _winningStrategy.Determine(bets);
                winnerBet.AssingAsWinner();
                await _lotStateService.AddNewStatusInLotAsync(lot, StatusLot.Completed);
            }
        }
    }
}
