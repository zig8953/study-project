import React, { Fragment } from 'react';
import { Navbar, FormControl, Button, Form, Container } from 'react-bootstrap'
import './Menu.css';
import logo from './Logo.png';
import { ModalConsumer } from '../../context/modal';
import LotCardsView  from '../../pages/LotCardsView';

import { ApiService } from '../../services/ApiService';
import { AuthService } from '../../services/AuthService';

import { Registration } from '../Modal/index';
import { toast } from 'react-toastify'
import { Link } from 'react-router-dom';


export default class AppContent extends React.Component<any, any> {
    authService: AuthService;
    apiService: ApiService;
    shouldCancel: boolean;
    UserName: string;

    constructor(props: any) {
        super(props);

        this.authService = new AuthService();
        this.apiService = new ApiService();
        this.state = { user: {}, api: {} };
        this.shouldCancel = false;
        this.UserName = "";
    }

    componentDidMount() {
        this.getUser();
    }

    login = () => {
        this.authService.login();
    };

    callApi = () => {
        this.apiService
            .callApi()
            .then(data => {
                this.setState({ api: data.data });
                toast.success('Api успешно возвращает данные, проверьте раздел - ответ API');
            })
            .catch(error => {
                toast.error(error);
            });
    };

    componentWillUnmount() {
        this.shouldCancel = true;
    }

    renewToken = () => {
        this.authService
            .renewToken()
            .then(user => {
                toast.success('Токен успешно продлен.');
                this.getUser();
            })
            .catch(error => {
                toast.error(error);
            });
    };

    logout = () => {
        this.authService.logout();
    };

    getUser = () => {
        this.authService.getUser().then(user => {
            if (user) {
                this.UserName = user.profile.name;
                toast.success('Пользователь успешно авторизован.' + user.profile.name);
            } else {
                this.UserName = "";
                toast.info('Вы не авторизованы.');
            }

            if (!this.shouldCancel) {
                this.setState({ user });
            }
        });
    };

    render() {
        if (this.UserName == "")
            return (
                <>
                    <ModalConsumer>
                        {({ showModal }) => (
                            <Fragment>
                                <Navbar.Brand onClick={() => showModal(Registration)}>Регистрация</Navbar.Brand>
                                <Navbar.Brand onClick={this.login}>Войти</Navbar.Brand>
                            </Fragment>
                        )}
                    </ModalConsumer>

                </>)
        else
            return (
                <>
                    <Navbar.Brand>{this.UserName}</Navbar.Brand>
                    <Navbar.Brand onClick={this.logout}>Выход</Navbar.Brand>
                </>)
    }
}

export const Menu = () => {
    return (
        <Container >
            <Navbar fixed="top" bg="light" variant="light">
                <div class="navbar-brand">
                    <a href="/home"> <img src={logo} href=""></img></a>

                </div>
                <div class="collapse navbar-collapse">
                    <Form inline>
                        <FormControl type="text" placeholder="Найдите любые товары" className="mr-sm-1" />
                        <Button variant="outline-primary">Поиск</Button>
                    </Form>
                </div>
                <div class="collapse navbar-collapse">
                    <Link to="/LotCardsView" class="auction-links">Лоты</Link>
                </div>
                <div class="collapse navbar-collapse">
                    <Link to="/MyListBets" class="auction-links">Мои ставки</Link>
                </div>
                <div class="form-inline my-2 my-lg-0">
                    <ModalConsumer>
                        {({ showModal }) => (
                            <Fragment>
                                <AppContent />
                            </Fragment>
                        )}
                    </ModalConsumer>
                </div>
            </Navbar>

        </Container>
    )
}