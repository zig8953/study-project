﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories.Files;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services.Files;
using ZhyveDotnet.AuctionThings.Core.Domain.Files;

namespace ZhyveDotnet.AuctionThings.Core.Application.Services.Files
{
    public class FileDocumentService
        : IFileDocumentService
    {
        private readonly FileDocumentSettings _fileDocumentSettings;
        private readonly IFileDocumentRepository _fileDocumentRepository;
        private readonly ILogger<FileDocumentService> _logger;

        public FileDocumentService(
            FileDocumentSettings fileDocumentSettings,
            IFileDocumentRepository fileDocumentRepository,
            ILogger<FileDocumentService> logger)
        {
            _fileDocumentSettings = fileDocumentSettings;
            _fileDocumentRepository = fileDocumentRepository;
            _logger = logger;
        }

        public async Task<FileDocument?> CreateAsync(string fileName, Stream stream, Guid userId)
        {
            var file = new FileDocument(fileName, userId);
            if (await UploadAsync(file.LocalName, stream).ConfigureAwait(false))
            {
                return await _fileDocumentRepository.CreateAsync(file).ConfigureAwait(false);
            }

            return null;
        }

        public async Task<FileDocument?> GetFileAsync(Guid id)
        {
            return await _fileDocumentRepository.GetByIdAsync(id).ConfigureAwait(false);
        }

        public async Task<IReadOnlyList<FileDocument>> GetFileCollectionAsync(FileDocumentFilter? filter)
        {
            return await _fileDocumentRepository.GetCollectionAsync(filter).ConfigureAwait(false);
        }

        public async Task<Stream?> GetFileContentAsync(FileDocument file, string privateKey)
        {
            if (file?.PrivateKey != privateKey)
            {
                return null;
            }

            var memory = new MemoryStream();

            await using var fileStream = File.OpenRead(GetFilePath(file.LocalName));
            await fileStream.CopyToAsync(memory).ConfigureAwait(false);

            memory.Seek(0, SeekOrigin.Begin);
            return memory;
        }

        public async Task UpdateAsync(Guid id, string fileName, Stream stream)
        {
            var file = await _fileDocumentRepository.GetByIdAsync(id).ConfigureAwait(false);
            if (file == null)
            {
                return;
            }

            file = file.Rename(fileName);

            if (await UploadAsync(file.LocalName, stream).ConfigureAwait(false))
            {
                await _fileDocumentRepository.UpdateAsync(file).ConfigureAwait(false);
            }
        }

        private async Task<bool> UploadAsync(string localName, Stream stream)
        {
            if (stream?.CanRead != true)
            {
                return false;
            }

            stream.Seek(0, SeekOrigin.Begin);

            return await UploadItemAsync(GetFilePath(localName), stream).ConfigureAwait(false);
        }

        private async Task<bool> UploadItemAsync(string filePath, Stream stream, int count = 0)
        {
            var result = false;

            if (count < _fileDocumentSettings.CountUploadTries)
            {
                try
                {
                    var directoryPath = Path.GetDirectoryName(filePath);
                    if (!Directory.Exists(directoryPath))
                    {
                        Directory.CreateDirectory(directoryPath);
                    }

                    await using var fileStream = File.OpenWrite(filePath);
                    await stream.CopyToAsync(fileStream).ConfigureAwait(false);

                    result = true;
                }
                catch (Exception e)
                {
                    _logger.LogWarning(
                        $"Не удалось создать/обновить файл {Path.GetFileName(filePath)} с попытки #{count + 1}", e);
                    await Task.Delay(1_000);

                    stream.Seek(0, SeekOrigin.Begin);
                }

                if (!result)
                {
                    result = await UploadItemAsync(filePath, stream, ++count).ConfigureAwait(false);
                }
            }

            return result;
        }

        private string GetFilePath(string localName) => Path.Combine(_fileDocumentSettings.StoragePath, localName);
    }
}