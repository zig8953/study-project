﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Application.Validators
{
    public class LotValidator : AbstractValidator<Lot>
    {
        private readonly IUserRepository userRepository;

        public LotValidator(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
            RuleFor(lot => lot.Name).NotEmpty().WithMessage("Имя лота не может быть пустым!");

            RuleFor(lot => lot.UserOwnerId)
                .NotEmpty()
                .WithMessage("Необходимо указать владельца лота!")
                .MustAsync((id, token) => UserIsAvailableAsync(id))
                .WithMessage("По указанному Id пользователь отсутствует либо был удален!");

            RuleFor(lot => lot.InitialCost)
                .Must(cost => cost > 0)
                .WithMessage("Стартовая цена лота должна быть больше 0!");

            RuleFor(lot => lot.Currency).Must(c => c != Currency.NotSet).WithMessage("Необходимо указать валюту, в которой будут проводиться торги!");
        }

        private async Task<bool> UserIsAvailableAsync(Guid userId)
        {
            var user = await userRepository.GetByIdAsync(userId);
            return user != null && !user.IsDeleted;
        }
    }
}
