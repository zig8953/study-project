﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories.Files;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Domain.Files;

namespace ZhyveDotnet.AuctionThings.DataAccess.Repositories.Files
{
    public class FileDocumentRepository : IFileDocumentRepository
    {
        private readonly DataContext _dataContext;

        private DbSet<FileDocument> FileDocumentsDbSet => _dataContext.Set<FileDocument>();

        public FileDocumentRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<IReadOnlyList<FileDocument>> GetAllAsync()
        {
            return await GetCollectionAsync(filter: null).ConfigureAwait(false);
        }

        public async Task<FileDocument?> GetByIdAsync(Guid id)
        {
            return await FileDocumentsDbSet.FirstOrDefaultAsync(x => x.Id == id).ConfigureAwait(false);
        }

        public async Task<FileDocument?> GetItemAsync(FileDocumentFilter? filter)
        {
            return await this.GetFileDocuments(filter).FirstOrDefaultAsync().ConfigureAwait(false);
        }

        public async Task<IReadOnlyList<FileDocument>> GetCollectionAsync(FileDocumentFilter? filter)
        {
            return await this.GetFileDocuments(filter).ToArrayAsync().ConfigureAwait(false);
        }

        public async Task<FileDocument> CreateAsync(FileDocument value)
        {
            await FileDocumentsDbSet.AddAsync(value).ConfigureAwait(false);
            await _dataContext.SaveChangesAsync().ConfigureAwait(false);

            return value;
        }

        public async Task UpdateAsync(FileDocument value)
        {
            FileDocumentsDbSet.Update(value);
            await _dataContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task DeleteAsync(Guid id)
        {
            FileDocument? fileDocument = await _dataContext.Set<FileDocument>().SingleOrDefaultAsync(x => x.Id == id);
            if (fileDocument == null)
            {
                throw new EntityNotFoundException($"Файл по id {id} не найден.");
            }

            _dataContext.Remove(fileDocument);
            await _dataContext.SaveChangesAsync();
        }

        private IQueryable<FileDocument> GetFileDocuments(FileDocumentFilter? filter = null)
        {
            IQueryable<FileDocument> files = FileDocumentsDbSet;

            if (filter != null)
            {
                if (filter.Ids != null)
                {
                    files = files.Where(x => filter.Ids.Contains(x.Id));
                }

                if (filter.LotIds != null)
                {
                    files = files.Where(x => filter.LotIds.Contains(x.LotId));
                }

                if (!string.IsNullOrEmpty(filter.PartOfFileName))
                {
                    files = files.Where(x => x.PublicName.Contains(filter.PartOfFileName));
                }

                if (filter.IsDeleted.HasValue)
                {
                    files = files.Where(x => x.IsDeleted == filter.IsDeleted.Value);
                }
            }

            return files;
        }
    }
}