﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T : BaseEntity
    {
        Task<IReadOnlyList<T>> GetAllAsync();

        Task<T?> GetByIdAsync(Guid id);

        Task<T> CreateAsync(T value);

        Task UpdateAsync(T value);

        Task DeleteAsync(Guid id);
    }
}